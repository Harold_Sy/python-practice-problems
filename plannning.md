# Planning

## Research

* [ ] Vocab
* [ ] Functions
* [ ] Methods

## Problem decomposition

* [ ] Input
* [ ] Output
* [ ] Examples
* [ ] Conditions (if)
* [ ] Iteration (loop)

## Problems


### 01 minimum_value (problem_001)
takes in two numbers from outside of the function
if one number is bigger than another then return that number
other wise return the other number
    note to self this is a good place to use 'min()' function


### 02 max_of_three (problem_004)
takes in three numbers from outside of the function
if one number is larger than the other two numbers then return that number
other wise if one of the other numbers is larger than the other numbers then return that number
else return the last number
    note to self this is a good place to use 'max()' function


### 03 sky_dive (problem_006)
takes in two data, one is an integer representing age and the other is a string representing yes or no.
if the age is 18 or greater and they have a permission slip then they can go sky diving
else then cannot

### 04 is_palindrome (problem_009)
takes in a string with no spaces.
returns True if the string reads the same forward and backwards
return if string== the reverse of the string

### 05 is_divisible_by_3 (problem_010)
takes in an integer and returns True if it is divisible by three other wise False
just return number mod 3 equality check to 0 and it should return True or False

### 06 is_divisible_by_5 (problem_011)
takes in an integer and returns True if it is divisible by five other wise False
just return number mod 5 quality check to 0 and it shoud return True or False

### 07 fizzbuzz (problem_012)
takes in an integer and return fizzbuzz if the number is divisible by 3 and 5
it return fizz if the number is divisible by 3 and returns buzz if the number is divisible by 5
return the number if it is not divisible by both three and five or both
i would test out if it is divisible by three and five first (return a fizzbuzz)
then I would test if it is divisible by only three (return fizz)
then I would test if it is divisible by only five (retrun buzz)
then at this point just return the number


### can_make_pasta (problem_014)
takes in a list of three items and checks to see if a particular three items is in the list
if so it returns a True meaning you can make pasta else a false
check to see of the list contains all the items then return True
else False

### is_inside_bounds(problem_016)
take in two integers representing x and y coordinates and checks if the location is within bounds
check to see if the x and y are within the number 10 and below but above or equal to zero
return true if so other wise false

### is_inside_bounds (problem_019)
take in six integers representing coordinates
returns true if all the following is true else false
x is greater than or equal to rect_x
y is greater than or equal to rect_y
x is less than or equal to rect_x + rect_width
y is less than or equal to rect_y + rect_height


### has_quorum(problem_20)
take two lists and compares one to the other to check if more than 50% of the list is in the other
if so it returns True else False


### gear_for_day (problem_022)
takes in workday (string reprenting yes or no) and sunny day (string representing yes or no)
if the day is a work day then
   if the day is sunny then (laptop)
   else laptop and umbrella
   if sunny and not a workday then (surfboard)
   else (surfboard)


### calculate_average (problem_024)
takes in a list of integers or an empty list
returns the average if numbers are in the list
returns None if the list is empty
return None


### calculate_sum (problem_025)
takes in an list of numbers and returns the sum of all the numbers or None if the list is empty
if the list is empty return None
return the of the list


### calculate_grade (problem_026)
takes in the a list of scores
calculate the average
return the correct grade
if 90> return 'a'
elif 80> return 'b'
elif 70> return 'c'
elif 60> return 'd'
return 'f'



### max_in_list (problem_027)
takes a list of numbers and returns the biggest number
if no number present (if the list is empty return None)
I could use the max() function here but lets do this the
hard way. we will loop through the list keeping a record of
the largest number until the end of the program


### remove_duplicate_letters (problem_028)
takes a string and returns the string with duplicate letters removed
return None if string is empty
    create a new list
    check if item is in the list if not then append it
    (rinse and repeat until the list is completed)
    return list as a string by using ''.join()
    thought: I wonder if I could convert a list into a string? Nope!

### find_second_largest (problem_030)
takes in a list of numbers
we have to find the second largest number and return that
we can use tow variables largest and second largest to keep track of our numbers
in the end there is a possiblity of the second largest being the largest so we have to
check for that and swap them
using two if statements should do the job
loop here
if item is larger that largest:
    put what ever is in largest as the second largest
    then put the item in the largest
else
    check if the item is larger than the second largest
    if it is then second_largest=item
return the second largest


###  sum_of_squares (problem_031)
take a list of numbers and returns the sum of each item squared
if the list is empty return None
read each number and multiply it to itself then add it to the next one in line
return the total


### sum_of_first_n_numbers (problem_032)
take in a number (integer) and returns the sum from zero to the number inclusive.
if the value limit is less than 9 then it should return None.
set a variable to zero
loop from 0 to the number+1 (to include the number)
   variable+=number from loop
return vatiable value
     

### sum_of_first_n_even_numbers (problem_033)
takes in a number (integer) and returns the sum from 0 to the number (only the even numbers!)
if the number taken in is less than 0 then return None.
set sum variable to zero.
loop from 0 to the number.
   if the number is even then add it to sum
return sum


### count_letters_and_digits (problem_034)
take in a string and returns two integers representing the count of letters and numbers.
we need counters so we set counters for letters and numbers to zero.
loop: we traverse the string getting the individual characters.
     if the item is a number we increment the number counter
     if the item is a letter we increment the letter counter
return the two counters

### count_letters_and_digits (problem_035)
looks like this problem and the previous one is the same.


### pad_left (problem_037)
takes in two integers and a character for padding and returns a new string.
the total length of the new string is the number that is passed.
find the length of the number as a string (number length)
set total length to the length number that was passed and minus the number length from that
pad the new string with the character that was passed up to the length.
then add the number as a string at the end.
return the new string. 


### reverse_dictionary (problem_039)
take in a dictionary and swap the keys for the values and return the new dictionary.
create a temporary dictionary.
loop: traverse through the dictionary (getting the keys).
     place the value in as a key and the key as a value in the new dictionary
return new dictionary.

### add_csv_lines (problem_041)
takes in a list of strings representing digits.
we have to return a new list with the digits all added up to one another per string
(I was under the assumption that split() does not split a string into individual characters like ruby does)
will use list conversion instead.
loop: traverse the list pulling out each string.
     convert the string into a list of all the characters into a list
     loop: traverse the new list getting each character.
           get the sum by converting the characters into numbers and keeping a running total
     put the number back into the same list as a number (no conversion)
return the list

### pairwise_add (problem_042)
takes in two lists of the same size and returns a new list with corresponding rows added.
we have to use the zip() function for this one.
use zip to create a list of tuples from the two lists.
loop: traverse to the list of tuple
    put the 'sum()' of each item in the tuple into a new list
return the new list


### find_indexes (problem_043)
takes in two parameters a list of numbers and a number to search for.
searches through the list for the item and returns the index in a list if found.
if not it returns an empty list
enumerate the list into another object (enum_list)
traverse the enumerated list checking the second index for the number then returning the first for the index
return the index as a list.


### translate (problem_044)
takes in two parameters a list of keys and a dictionary and returns a list with values for the keys.
loop: iterate over the keys.
     use key to find the value and put value in a new list if no value put None
return the new list


### make_sentence (problem_046)
takes in three lists with subjects, verbs, objects.
we must get the program to make sentences by putting them together as a string in the order of subject,verb,object
loop: traverse the first list grabbing an item.
    loop: traverse the next list grabbing an item.
        loop: traverse the last list grabbing an item.
             put all three items in the new list as a sentence.
return new list

### check_password (problem_047)
takes in a string returns True if the string represents a password else False.
create helper functions to help check for the different parts of the criteria
loop: traverse the string and grabs a character
       if any of the criteria comes out as false then return false
return True

### word_count_frequencies (problem_048)
words = split the sentence
counts = new empty dictionary
for each word in words
 if the word is not in counts
    counts[word] = 0
    add one to counts[word]
    return counts


### sum_two_numbers (problem_049)
takes in two numbers and returns the sum
take in both numbers and return the sum


### halve_the_list (problem_050)
takes in a list of numbers that can have odd or even amount of elements.
create two new empty lists.
split the length of the list and store number in (length).
if the length is odd then add 1 to the length to compensate for the extra element we want in the first list.
loop: read the elements and put them in the second list if the first one exeeds the size of length.
      else put the elements in the first list.
return the two new lists

### safe_divide (problem_051)
takes in two values a numerator and a denominator.
if the denominator is zero return math.inf else return numerator/denominator
import math

### generate_lottery_numbers (problem_052)
take in no parameters and returns a list of six unique random numbers from 1 to 40
import random module
setup an empty list
while the list length is less than six: loop
      get random number
      if random number is not in the list then
            put it in the list
return new list

### username_from_email (problem_053)
takes in a string that is a valid email address.
returns the username portion from the email address (the substring that appears before the @).
slice would be great for this.
if the string is empty (if not string) return "basia"
find the index of the @ symbol.
return the slice of 0 to the @ symbol

### check_input (problem_054)
takes any value then returns a valueerror when raise is entered otherwise returns the original value


### simple_roman(problem_055)
takes in an integer and returns the equivelent number in roman numerals.
create a dictionary.
return the integer as key in the dictionary giving us the value

### num_concat (problem_056)
takes in two numerical parameters returns the concatenation of the numbers as a string.
return str(para1)+str(para3)


### sun_fraction_sequence (problem_057)
takes in a number returns the sum in the form of 1/(num+1)
set up sum to handle the sum
iterate over the range 1 to the number passed+1:
      increment sum with the running total
return the sum

### group_cities_by_state (problem_058)
takes in a list of cities in the format "city, state" returns a dictionary using the state as the key.
loop: interate over the original list.
    

### specific_random (problem_059)
take no parameters. return a random number between 10 to 500 inclusive that is divisible by both 5 and 7.
import random module (need to use random.choice) random.choice can work with range().
use a while loop: while false: not 5 not 7
    get random number using random.choice on range(10,500+1)
return random number 

### only_odds (problem_060)
takes in a list of numbers and returns a list containing only the odd numbers
setup a new list
create a loop to interate over each item.
loop: get item
   if item is odd put in new list
return new list

### remove_duplicates (problem_061)
takes in a list of values and returns a list without duplicate items.
create a new list.
iterate over old list and get item:
     if item not in the new list
          then put in the new list
return new list

### basic_calculator (problem_062)
takes in two numbers left and right and the operation then returns the answer.
plugged in the three parameters into a list.
unpacked the list into three variables.
used if condition to process the operations and return the answer


### shift letters (problem_063)
takes in a string containing a single word. a new string replaced with the next letter in the alphabet.
(not sure what the letter is after 'z' but i dont think it is 'a'. so a better way to do this is to use strings)
set up a string with all the capital letters in it.
set up another string with all the lower case letters in it.
set up a new empty string that we will concatenate the new letters in.
iterate over the original string getting a letter.
      if the letter we got is uppercase then
           get the index of the old character plus 1 but mod the length of the string so that we can
           start at the beginning again if we end up at the end of the string. place the answer in the new string
      else
           we do the same thing but to the lowercase string and place the answer in the new string
return the new string

### temerature_differences (problem_064)
takes in two lists one with list of high temps and the other low temps. return list with difference between temps.
set up a new list.
iterate over both lists by iterating over a list index then getting the numbers.
       subtract the two numbers and put them in the new list.
return the new list

### biggest_gap (problem_065)
takes in a list of numbers at least two numbers in it. return the largest gap between two of the numbers.
set up a variable biggestGap=0
iterate over the indices for the list:
      make sure we pass at least the first item so we can compare two.
            get the two numbers and minus them from each other getting the abolute value
            if the answer is larger than the biggestGap then replace the number in that variable
return the biggestGap variable

