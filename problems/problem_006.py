# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# takes in an integer age and a string y or n
# returns a message if the person can go skydiving
# or not 
def can_skydive(age, has_consent_form):
    if age>=18 and has_consent_form=='y':
        return 'yes you can sky dive!'
    return 'too bad so sad.'

print(can_skydive(18,'y'))
print(can_skydive(16,'y'))
print(can_skydive(22,'n'))
print(can_skydive(6,'n'))