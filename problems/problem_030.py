# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    largest=0
    second_largest=0
    for item in values:
        if item>largest:
            second_largest=largest
            largest=item
        elif item>second_largest:
            second_largest=item
    return second_largest        

print(find_second_largest([13,1,6,4,9,8,20])) # second largest at the beginning
print(find_second_largest([20,8,9,4,6,1,13])) # second largest at the end
print(find_second_largest([1,4,8,13,6,9,20 ]))# second largest at the center
print(find_second_largest([20,9,6,13,8,4,1]))