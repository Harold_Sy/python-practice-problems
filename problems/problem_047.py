# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"



def check_password(password):
    is_digit=False
    is_alpha=False
    is_lower=False
    is_upper=False
    special_char=False
    for char in password:
        if char.isdigit():
            is_digit=True
        elif char.isalpha():
            is_alpha=True
            if char.isupper():
                is_upper=True
            else:
                is_lower=True
        elif char in '!@$':
            special_char=True

    return is_digit and is_alpha and is_upper and is_lower and special_char and len(password)>=6 and len(password)<=12        
            


print(check_password('Harold@a.sy1'))
print(check_password('harold@a.sy1'))
print(check_password('HAROLD@A.SY1'))
print(check_password('Haroldda.sy1'))
print(check_password('Harold@a.syy'))
print(check_password('har'))
print(check_password('Harold@a.sy1111111'))
