# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    if not values:
        return None
    return sum(values)

print(calculate_sum([1,5,2,8,9,23,44]))
print(calculate_sum([2,4,6,8,10]))
print(calculate_sum([5,10,15,20,25]))
print(calculate_sum([]))