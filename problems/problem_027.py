# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    largest=0
    for num in values:
        if num>largest:
            largest=num
    return largest


print(max_in_list([1,5,10,22,66]))
print(max_in_list([22,55,44,88,99]))
print(max_in_list([99,22,55,33,77,55]))
print(max_in_list([12,6,3,8,9,12]))