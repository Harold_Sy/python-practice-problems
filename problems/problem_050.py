# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(list):
    list1=[]
    list2=[]
    length=len(list)//2
    if length%2!=0:
        length+=1

    for items in list:
        if len(list1)>=length:
            list2.append(items)
        else:
            list1.append(items)
    return list1,list2

print(halve_the_list([1, 2, 3, 4]))
print(halve_the_list([1, 2, 3]))