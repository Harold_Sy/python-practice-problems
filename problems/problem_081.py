# Write four classes that meet these requirements.
#
# Name:       Animal
#
# Required state:
#    * number_of_legs, the number of legs the animal has
#    * primary_color, the primary color of the animal
#
# Behavior:
#    * describe()       # Returns a string that describes that animal
#                         in the format
#                                self.__class__.__name__
#                                + " has "
#                                + str(self.number_of_legs)
#                                + " legs and is primarily "
#                                + self.primary_color
#
#
# Name:       Dog, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Bark!"
#
#
#
# Name:       Cat, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Miao!"
#
#
#
# Name:       Snake, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Sssssss!"


class Animal:
    def __init__(self,number_of_legs,primary_color):
        self.number_of_legs=number_of_legs
        self.primary_color=primary_color

    def describe(self):
        return ("has "+str(self.number_of_legs)+" legs and is primarily "+self.primary_color)



class Dog(Animal):
    def __init__(self,primary_color):
        self.number_of_legs=4
        self.primary_color=primary_color

    def speak(self):
        return "Bark!"



class Cat(Animal):
    def __init__(self,primary_color):
        self.number_of_legs=4
        self.primary_color=primary_color

    def speak(self):
        return "Meow!"


class Snake(Animal):
    def __init__(self,primary_color):
        self.number_of_legs=0
        self.primary_color=primary_color

    def speak(self):
        return "hissssssssssss"
    


animal1=Animal(2,"black")
print(animal1.number_of_legs)
print(animal1.primary_color)
print(animal1.describe())
print()
Fido=Dog("White")
print(Fido.number_of_legs)
print(Fido.primary_color)
print(Fido.speak())
print()
Gypsy=Cat("Grey")
print(Gypsy.number_of_legs)
print(Gypsy.primary_color)
print(Gypsy.speak())
print()
Mombo=Snake("Green")
print(Mombo.number_of_legs)
print(Mombo.primary_color)
print(Mombo.speak())