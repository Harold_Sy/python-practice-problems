# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    list=[]
    if is_workday=='y':
        list.append('laptop')
        if is_sunny=='n':
            list.append('umbrella')
    else:
        list.append('surfboard')
    return list

#                  workday    sunny
print(gear_for_day(   'y',     'y'))
print(gear_for_day(   'y',     'n'))
print(gear_for_day(   'n',     'y'))
print(gear_for_day(   'n',     'n'))
