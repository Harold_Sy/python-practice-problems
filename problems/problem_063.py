# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters(string):
    letters1='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    letters2='abcdefghijklmnopqrstuvwxyz'
    new_string=''
    for character in string:
        if character in letters1:
            new_string+=letters1[(letters1.index(character)+1)%len(letters1)]
        else:
            new_string+=letters2[(letters2.index(character)+1)%len(letters2)]
    return new_string

print(shift_letters("import"))
print(shift_letters("ABBA"))
print(shift_letters("Kala"))
print(shift_letters("zap"))