# Write a function that meets these requirements.
#
# Name:       safe_divide
# Parameters: two values, a numerator and a denominator
# Returns:    if the denominator is zero, then returns math.inf.
#             otherwise, returns numerator / denominator
#
# Don't for get to import math!

import math

def safe_divide(numer,denomi):
    if denomi==0:
        return math.inf
    return numer/denomi

print(safe_divide(25,5))
print(safe_divide(0,5))
print(safe_divide(23,0))